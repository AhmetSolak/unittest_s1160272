Vehicle waarden = Power in Kw / waarde van je auto en het bouwjaar.
PolicyHolder waarden = leeftijd, datum wanneer je je rijbewijs hebt behaald, schadevrije jaren en postcode


PremiumCalculationTest.cs file = 

Functies:

PremiumAmountPerYear_CalculationBasedOnZipCode: De premie moet worden uitgerekent op basis van je postcode. Bij deze unittest test ik of de risico opslag goed word uitgerekent.
Bij postcodes 10xx-35xx moet er een 5% risico opslag komen. Bij postcodes 36xx-44xx moet er een 2% risico opslag gerekent op het uiteindelijke bedrag.
De data die ik heb gebruikt bij deze test is: age, driversLicenseStartDate, PostalCode, No claimYears. 
De techniek die ik heb gebruikt is Theory. Theory zodat ik zelf de waardes kan invullen en kan checken. 
De fout die ik hierin ontdekte was dat een > verkeerd was. Het moest kleiner dan zijn ipv groter dan. Het is inmiddels veranderd in de sourcecode.

PremiumAmountPerYear_ShouldCalculateBasedOnDamageFreeYears: Deze unit test kijkt of je korting krijgt op basis van je schadevrije jaren.
Bij 5 jaar krijg je nog de basis prijs, op de 6e jaar hoor je een extra 5% korting te krijgen, dit gaat zo door tot maximaal 65%. Bij 18 jaar ben je dan op de maximale 
korting. Ook heb ik de 19 jaar getest om tekijken of het niet verder gaat dan 65%
De data die ik heb gebruikt bij deze test is: Aantal schadevrije jaren. Dit heb ik omgezet naar een double. Ook heeft die alle vehicle waardes nodig, denk aan Kw, waarde
en bouwjaar van een auto. Vervolgens hebben we ook de data nodig van degene die het aanvraagt, oftewel de policyHolder(leeftijd, wanneer rijbewijs behaald, schadevrije jaren
en postcode)
De premium uitrekening heb ik gebasseerd op de jaarlijkse kosten ipv de maandelijkse.

PremiumPaymentAmountALLRISK_PaymentPerYear_ShouldCalculatePrice: Bij deze functie heb ik getest of de ALL RISK betaling per jaar goed uitgerekent word. Hiervoor heb ik 
alle Vehicle waarden gebruikt en ook de policyHolder waarden. (zie bovenaan). De techniek is een Fact, die ik heb gebruikt om achtertehalen of hij de berekening goed uitrekent.

PremiumPaymentAmountWAPLUS_PaymentPeriodMonth_ShouldCalculatePrice: Bij deze functie heb ik getest of de WAPLUS betaling per maand goed word uitgerekent. Hiervoor heb ik
alle Vehicle waarden gebruikt en ook de policyHolder waarden. (zie bovenaan). De tecniek is een Fact, die ik heb gebruikt om achtertehalen of hij de berekening goed uitrekent.
Dit is in principe precies hetzelfde test, als de functie hierboven, maar dan met andere formules voor de berekening van de WAPLUS/Monthly payments


PolicyHolderTest.cs file = 

Functies:

ShouldThrowExceptionWithMessage: Bij deze functie heb ik getest dat wanneer je een negatieve waarde invoert bij je leeftijd, je een message krijgt met "Age should be postive".
Ook heb ik getest dat wanneer je verkeerde waardes bij je datum invoert, denk aan 25/03/12231, het opnieuw vraagt om het correct in te voeren: "Date format is not valid".
De waarden die ik hiervoor heb gebruikt is de leeftijd en het datum van wanneer je je rijbewijs hebt behaald. Dit heb ik gedaan met een Theory check zodat ik zelf de waarden
kan invullen en eventueel kan kijken of het werkt. Eerst gaf deze test nog een error, nadat ik de error heb opgelost met de message, gaf hij een geslaagde unit test.


VehicleTest.cs file =

ShouldThrowExceptionWithMessage: Bij deze functie heb ik getest dat wanneer je een negatieve waarde invoert bij de vehicle waarden, je een error krijgt met:
"The value should be positive. Please try again". Dit heb ik bij elke vehicle waarden apart getest. De waarden die ik hiervoor hebt gebruikt zijn dus de vehicle waarden
(zie bovenaan de file) Dit heb ik gedaan met een Theory check zodat ik zelf de waaardne kan invullen en eventueel kan kijken of het werkt. Eerst gaf deze test nog een error,
maar nadat ik de source code heb veranderd, gaf hij de message.

Ook heb ik kleine dingen aangepast in de code, zoals een paar dingen naar double zetten, een groter dan teken veranderen voor het uitrekenen van de premium.





