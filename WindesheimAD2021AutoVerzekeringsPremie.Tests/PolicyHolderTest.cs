using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremie.Tests
{
    public class PolicyHolderTest
    {

        [Theory]
        [InlineData(-33, "01/06/2011", "Age should be positive")] // this is to test the case when you supply a negative age
        [InlineData(20, "25/03/12231", "Date format is not valid")] // this is to test the case when invalid date is supplied
        public void ShouldThrowExceptionWithMessage(int Age, string driverlicenseStartDate, string message)
        {

            var ex = Assert.Throws<Exception>(()=>
            {
                var policyHolder = new PolicyHolder(Age, driverlicenseStartDate, 1067, 3);
            });

            Assert.Equal(message, ex.Message);
        }
    }
}
