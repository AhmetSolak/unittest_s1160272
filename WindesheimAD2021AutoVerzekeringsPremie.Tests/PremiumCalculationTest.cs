﻿using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;
using static WindesheimAD2021AutoVerzekeringsPremie.Implementation.PremiumCalculation;

namespace WindesheimAD2021AutoVerzekeringsPremie.Tests
{
    public class PremiumCalculationTest
    {

        // this checks if the zipcode effects the risk/
        [Theory]
        [InlineData(18, "09/10/2014", 1040, 2, 27.37)] // in zipcode range 10xx - 35xx - 5% in calculation
        [InlineData(31, "09/10/2014", 3500, 2, 23.8)] // in zipcode 3500 - 5% in calculation
        [InlineData(31, "09/10/2014", 3600, 2, 23.12)] // in zipcode 3600 - 2% in calculation
        [InlineData(31, "09/10/2014", 3650, 2, 23.12)] // in zipcode range 36xx - 44xx - 2% in calculation
        public void PremiumAmountPerYear_CalculationBasedOnZipCode(int age, string DriversLicenseStartDate, int PostalCode, int NoClaimYears, double expected)
        {
            Vehicle vehicle = new Vehicle(200, 3000, 2019);
            PolicyHolder policyHolder = new PolicyHolder(age, DriversLicenseStartDate, PostalCode, NoClaimYears);
            PremiumCalculation premiumCalculation = new(vehicle, policyHolder, InsuranceCoverage.WA);

            double actual = premiumCalculation.PremiumAmountPerYear;

            Assert.Equal(expected, actual);
        }


        // Checks if discount based on damage free years is calculated correctly
        [Theory]
        [InlineData(5, 23.8)] // base
        [InlineData(6, 22.61)] // 5% bracket
        [InlineData(8, 20.23)] // 15% bracket
        [InlineData(18, 8.33)] // 65% bracket (max)
        [InlineData(19, 8.33)] // 65% (doesnt go over max)
        public void PremiumAmountPerYear_ShouldCalculateBasedOnDamageFreeYears(int noClaimYears, double expected)
        {
            var vehicle = new Vehicle(200, 3000, 2019);
            var policyHolder = new PolicyHolder(31, "09/10/2014", 1040, noClaimYears);
            var premiumCalculation = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA);

            var actual = premiumCalculation.PremiumAmountPerYear;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PremiumPaymentAmountALLRISK_PaymentPerYear_ShouldCalculatePrice() //This test if it calculates your yearly payment in ALL RISK.
        {
            var vehicle = new Vehicle(210, 4500, 1999);
            var paymentPeriod = PaymentPeriod.YEAR;
            var policyHolder = new PolicyHolder(35, "13/08/2014", 1078, 0);
            var premiumCalculation = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.ALL_RISK);
            var expected = 44.39;


            var actual = premiumCalculation.PremiumPaymentAmount(paymentPeriod);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PremiumPaymentAmountWAPLUS_PaymentPeriodMonth_ShouldCalculatePrice() //This test if it calculates your monthly payment in WA PLUS.
        {
            var vehicle = new Vehicle(210, 4500, 1999);
            var paymentPeriod = PaymentPeriod.MONTH;
            var policyHolder = new PolicyHolder(35, "06/11/2014", 1045, 0);
            var premiumCalculation = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA_PLUS);
            var expected = 2.28;


            var actual = premiumCalculation.PremiumPaymentAmount(paymentPeriod);

            Assert.Equal(expected, actual);
        }
    }
}
