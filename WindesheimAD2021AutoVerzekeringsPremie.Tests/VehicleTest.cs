﻿using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremie.Tests
{
    public class VehicleTest
    {

        [Theory]
        [InlineData(200, 4000, 2019, 2)]
        [InlineData(300, 4000, 2022, 0)]
        public void ShouldCalculateTheRightAge(int powerInKw, int valueInEuros, int constructionYear, double expected)
        {
            var vehicle = new Vehicle(powerInKw, valueInEuros, constructionYear);

            Assert.Equal(expected, vehicle.Age);
        }

        [Theory]
        [InlineData(-18, 0, 1, "The value should be positive. Please try again") ] // this is to test the case when you put a negative number in power in Kw
        [InlineData(5, -0, 4, "The value should be positive. Please try again") ] // this is to test the case when the value is a negative number
        [InlineData(5, 1000, -1, "The value should be positive. Please try again")] // this is to test the case when the construction year is a negative number.
        [InlineData(-5, -1000, -4, "The value should be positive. Please try again")] // this is to test the case when every data is a negative number.
        public void ShouldThrowExceptionWithMessage(int powerInKw, int valueInEuros, int constructionYear, string message)
        {

            var ex = Assert.Throws<Exception>(() =>
            {
                var vehicle = new Vehicle(powerInKw, valueInEuros, constructionYear);
            });

            Assert.Equal(message, ex.Message);
        }
    }
}