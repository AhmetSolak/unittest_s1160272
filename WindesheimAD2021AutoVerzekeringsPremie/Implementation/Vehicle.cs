﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace WindesheimAD2021AutoVerzekeringsPremie.Implementation
{
    class Vehicle
    {
        public int PowerInKw { get; private set; }
        public int ValueInEuros { get; private set; }
        public int Age { get; private set; }
       

        internal Vehicle (int PowerInKw, int ValueInEuros, int constructionYear)
        {
           
            if (IsValidPowerInKw(PowerInKw))
            {
                this.PowerInKw = PowerInKw;
            }
            else
            {
                throw new Exception("The value should be positive. Please try again"); // this error will be fired when the number is negative
            }

            if (isValidValueInEuros(ValueInEuros))
            {
                this.ValueInEuros = ValueInEuros;
            }
            else
            {
                throw new Exception("The value should be positive. Please try again"); // this error will be fired when the number is negative
            }

            if (isValidValueConstructionYear(constructionYear))
            {
                this.Age = constructionYear;
            }
            else
            {
                throw new Exception("The value should be positive. Please try again"); // this error will be fired when the number is negative
            }

            this.PowerInKw = PowerInKw;
            this.ValueInEuros = ValueInEuros;
            Age = constructionYear > DateTime.Now.Year ? 0 : DateTime.Now.Year - constructionYear;            
        }
        public static bool IsValidPowerInKw(int PowerInKw)
        {
            return PowerInKw > 0;
        }
        public static bool isValidValueInEuros(int ValueInEuros)
        {
            return ValueInEuros > 0;
        }
        public static bool isValidValueConstructionYear(int constructionYear)
        {
            return constructionYear > 0;
        }

    }
}
